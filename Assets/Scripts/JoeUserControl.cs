using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class JoeUserControl : MonoBehaviour
{
    public GameObject controlledUnit;
    private RunningCharacter m_Character;
    private bool m_moveRigthAction = false;
    private bool m_moveLeftAction = false;
    private bool m_JumpAction = false;
    private bool m_IncreaseEnergyAction = false;
    private bool m_IncreasePowerAction = false;

    public Sprite ExtraLifeIcon;

    private bool m_hasExtraLife = true;

    private void Awake()
    {
        m_Character = controlledUnit.GetComponent<RunningCharacter>();
    }


    private void Update()
    {
        if (m_Character == null)
        { // here respawn character or exit level
            if (!m_hasExtraLife) SceneManager.LoadScene(0);
            m_hasExtraLife = false;
        }
        else
        {
            transform.position = controlledUnit.transform.position;
            float xC = CrossPlatformInputManager.GetAxis("Horizontal");

            if (m_moveLeftAction && xC == 0) m_moveLeftAction = false;
            else if (xC < 0 && !m_moveLeftAction)
            {
                m_Character.Move(MoveDirection.left, "left");
                m_moveLeftAction = true;
                m_moveRigthAction = false;
            }

            if (m_moveRigthAction && xC == 0) m_moveRigthAction = false;
            else if (xC > 0 && !m_moveRigthAction)
            {
                m_Character.Move(MoveDirection.right, "right");
                m_moveRigthAction = true;
                m_moveLeftAction = false;
            }

            if (!m_JumpAction) m_JumpAction = CrossPlatformInputManager.GetButtonDown("Jump");
            else
            {
                m_Character.Move(MoveDirection.nowhere, "jump");
                m_JumpAction = false;
            }

            if (!m_IncreaseEnergyAction) m_IncreaseEnergyAction = CrossPlatformInputManager.GetButtonDown("IncreaseHealth");
            else
            {
                m_Character.Move(MoveDirection.nowhere, "increaseEnergy");
                m_IncreaseEnergyAction = false;
            }

            if (!m_IncreasePowerAction) m_IncreasePowerAction = CrossPlatformInputManager.GetButtonDown("IncreasePower");
            else
            {
                m_Character.Move(MoveDirection.nowhere, "increasePower");  
                m_IncreasePowerAction = false;
            }

        }
    }

   
}