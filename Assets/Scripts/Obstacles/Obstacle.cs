﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
	public GameObject smokePrefab;
	private GameObject m_go;

	void Start() {
		m_go = GetComponentInParent<WorldObject> ().gameObject;
	}

	void OnTriggerEnter(Collider other) {
		// apply damage to other this collide with
		var otherScript = other.GetComponentInParent<RunningCharacter>(); // TODO: replace with interface
		if (otherScript != null) {
			otherScript.takeDamage ();

			StartCoroutine (Collapse ());
		}
			

		// receive damage from physical objects
//		var otherRigidbody = collision.collider.GetComponent<Rigidbody> ();
//		var objectHits = otherRigidbody != null;
//		if (objectHits) {
//			var velocityIsEnought = collision.impulse.magnitude > k_impulseDamaging && otherRigidbody.velocity.magnitude > 5;
//			var nonhitting = otherRigidbody.GetComponent<Nonhitting> ();
//			var isSafeObject = nonhitting != null;
//			if (!isSafeObject && velocityIsEnought) {
//				var otherMass = otherRigidbody.mass;
//				var damage = (k_collisionDamage * otherMass) * (collision.impulse.magnitude / k_impulseDamaging);
//				receiveHit (damage);
//				//			print (string.Format ("receiving damage {0} from impulse {1} of mass {2}", damage, collision.relativeVelocity.magnitude, otherMass));
//				//			print (string.Format ("impulse: {0}", collision.impulse.magnitude));
//				//			print (string.Format ("velocity: {0}", collision.relativeVelocity.magnitude));
//			}
//		}
	}

	IEnumerator Collapse() {
		var smoke = Instantiate (smokePrefab) as GameObject;
		smoke.transform.position = transform.position;
		smoke.GetComponent<WorldObject> ().worldParameters = GetComponentInParent<WorldObject> ().worldParameters;

		yield return new WaitForSeconds (0.5f);

		m_go.SetActive(false);
	}
}
