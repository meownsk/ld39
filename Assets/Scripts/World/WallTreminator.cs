﻿using UnityEngine;
using System.Collections;

public class WallTreminator : MonoBehaviour {

	void OnTriggerExit(Collider another){
		var otherPoolable = another.GetComponentInParent<Poolable>();
		if (otherPoolable != null) {
			otherPoolable.ReturnToPool ();
		} else {
			Destroy (another.gameObject);
		}
	}
}
