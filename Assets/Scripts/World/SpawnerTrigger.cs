﻿using System;
using UnityEngine;


[RequireComponent(typeof(BoxCollider))]
public class SpawnerTrigger : MonoBehaviour {
	private BoxCollider m_collider;
	[SerializeField] private Transform m_lastSpawned = null;
	private bool m_empty = true;
	
	public delegate void TriggerEvent();
	public event TriggerEvent empty;

	void Awake() {
		m_collider = GetComponent<BoxCollider> ();
	}

	void OnTriggerExit(Collider other) {
		if (other.transform == m_lastSpawned || other.transform.IsChildOf(m_lastSpawned)) {
			if (empty != null)
				empty ();
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.transform == m_lastSpawned || other.transform.IsChildOf(m_lastSpawned)) {
			m_empty = false;
		}
	}

	public void Trigger() {
		if (empty != null && m_empty)
			empty();
	}

	public bool isEmpty {
		get {
			var layerMask = ~ (1<<2);
			var overlappedColliders = Physics.OverlapBox (transform.position + m_collider.center, m_collider.size / 2, Quaternion.identity, layerMask);

			var contained = Array.Exists (overlappedColliders,
				elem => {
					return elem.transform == m_lastSpawned || elem.transform.IsChildOf(m_lastSpawned);
				});
			return !contained;
		}
	}

	public void SpawnNext(Transform nextTile) {
		Vector3 nextTilePosition;

		if (m_lastSpawned != null) {
			var oldSizeComponent = m_lastSpawned.GetComponent<TileSize> ();
			var oldTileSize = oldSizeComponent.size.z;
			nextTilePosition = m_lastSpawned.position + new Vector3 (0, 0, oldTileSize / 2);
		} else {
			nextTilePosition = transform.position;
		}

		SpawnTile (nextTile, nextTilePosition.z);
	}

	public void SpawnTile(Transform tile, float zCoord) {
		Vector3 tilePosition;

		var sizeComponent = tile.GetComponent<TileSize> ();
		var tileSize = sizeComponent.size.z;

		tilePosition = new Vector3 (transform.position.x, transform.position.y, zCoord + tileSize / 2);

		tile.position = tilePosition;
		tile.rotation = transform.rotation;

		m_lastSpawned = tile;
	}
}
