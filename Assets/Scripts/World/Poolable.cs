﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poolable : MonoBehaviour {
	public GameObjectsPool poolManager;

	public void ReturnToPool() {
		poolManager.returnTile (transform);
	}
}
