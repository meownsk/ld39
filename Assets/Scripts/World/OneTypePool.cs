﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneTypePool : GameObjectsPool {
	public GameObject tilePrefab;

	override protected IEnumerator spawnPool() {
		for (int i = 0; i < poolSize; i++) {
			yield return null;
			tiles.Add (spawnTile(tilePrefab).transform);
		}

		TriggerFilledEvent ();
	}

	protected override GameObject spawnAdditional () {
		return spawnTile (tilePrefab);
	}
}
