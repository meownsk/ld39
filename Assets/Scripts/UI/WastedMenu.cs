﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WastedMenu : MonoBehaviour {
	[SerializeField] private AudioSource m_audio;

	public void StartBtnAction() {
		Debug.Log ("start btn hit");
		SceneManager.LoadScene (0);
	}

	public void ExitBtnAction() {
		Debug.Log ("exit btn hit");
		Application.Quit ();
	}

	public void ActivateMenu() {
		gameObject.SetActive (true);
		m_audio.Play ();
	}
}
