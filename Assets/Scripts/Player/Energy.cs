﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour {
	public float maxValue = 5;
	[SerializeField] private float current = 1;
	private float k_deltaForIncrease = 0.25f;
	private float k_deltaForDecrease = 1;
	private float k_stoppValue = 0;

    public delegate void TriggerEvent();
    public event TriggerEvent die;

    public void increase()
    {
        if (current < maxValue) current += k_deltaForIncrease;
    }

    public void decrease()
    {
        if (current >= k_stoppValue) current -= k_deltaForDecrease;
        if ((current <= k_stoppValue) && (die != null)) die();
    }

	public float currentValue {
		get {
			return current;
		}
	}
}
