﻿using UnityEngine;
using System.Collections;

public class PlayerState : MonoBehaviour {
    public RunningCharacter runningCharacter;
    public WorldParameters worldParameters;
    public JoeUserControl joeUserControl;

	[SerializeField] private GameUI m_ui;
	[SerializeField] private WastedMenu m_wastedMenu;

	private Score score;
	private Power power;
	private Energy energy;
   
	void Awake() {
		power = gameObject.AddComponent<Power>();
		energy = gameObject.AddComponent<Energy>();
		score = gameObject.AddComponent<Score>();
	}

    void Start() {
        power.die += die;
        energy.die += die;
        runningCharacter.movePlayer += movePlayer;
        runningCharacter.increaseEnergy += increaseEnergy;
		runningCharacter.decreaseEnergy += decreaseEnergy;
		runningCharacter.increasePower += increasePower;
    }

    private void Update () {
        score.increase(worldParameters);
       // Debug.Log("#score = " + score.getScore());
        if (score.isRound())
        {
            runningCharacter.startAnimBonus();
        }
    }

    //TODO:DIE
    void die() {
        score.saveResult();
		runningCharacter.die ();
		worldParameters.stop ();

		m_ui.gameObject.SetActive (false);
		m_wastedMenu.ActivateMenu ();
    }

    void movePlayer() {
        power.decrease();
//        Debug.Log("power decrease" + power.current);

    }

    public void decreaseEnergy() {
        energy.decrease();
//        Debug.Log("energy decrease" + energy.current);
		worldParameters.actionShock();
    }

    public void increaseEnergy()
    {
        energy.increase();
//        Debug.Log("energy increase" + energy.current);
    }

    public void increasePower()
    {
        power.increase();
//        Debug.Log("Power increase" + power.current);
    }

    public int getLastScore()
    {
        return score.lastScore;
    }

    public int getBestScore()
    {
        return score.bestScore;
    }
}
