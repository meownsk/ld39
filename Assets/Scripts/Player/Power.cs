﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour {
	public float maxValue = 5;
    [SerializeField] private float current = 3;
	private float k_deltaForIncrease = 0.25f;
	private float k_deltaForDecrease = 1;
	private float k_stopValue = 0;
    public float delta = 0;

    public delegate void TriggerEvent();
    public event TriggerEvent die;

    public void increase()
    {
        if (current < maxValue) current += k_deltaForIncrease;
    }

    public void decrease()
    {
        if ((current <= k_stopValue) && (die != null)) die();
        else current -= k_deltaForDecrease;
    }

	public float currentValue {
		get {
			return current;
		}
	}
}

