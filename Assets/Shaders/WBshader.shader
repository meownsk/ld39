﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/WBshader" {
	//     Properties {
	//        _Color ("Color", Color) = (0,0,0,1)
	//    }
	//    SubShader {
	//        Pass {
	// 
	//        CGPROGRAM
	//        #pragma target 3.0
	//        #pragma vertex vert
	//        #pragma fragment frag
	//        #include "UnityCG.cginc"
	//       
	//        struct v2f {
	//            float4 pos : SV_POSITION;
	//            float3 color : COLOR0;
	//        };
	//       
	//        struct V_IN
	//        {
	//            float4 vertex : POSITION;
	//            float3 normal : NORMAL;
	//            float4 texcoord : TEXCOORD0;
	//        };
	//       
	//        v2f vert (V_IN v)
	//        {
	//            v2f o;
	//            float4 nrml = float4(v.normal, 1);
	//            o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	//            o.color = nrml * 0.7 + 0.5;
	//            return o;
	//        }
	//       
	//        half4 frag (v2f i) : COLOR
	//        {
	//            return half4 (i.color, 1);
	//        }
	//        ENDCG
	//   
	//        }
	//    }
	//    Fallback "VertexLit"
	//}
	//	Properties {
	//		_Color ("Color", Color) = (0.3,0.3,0.3,1)
	//		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	//		_Detail ("Detail", 2D) = "gray" {}
	//	}
	//	SubShader {
	//		Tags { "RenderType"="Opaque" }
	////		pass {
	//		CGPROGRAM
	//		// Physically based Standard lighting model, and enable shadows on all light types
	//		#pragma surface surf Standard //fullforwardshadows
	////		#pragma vertex vert
	////		#pragma fragment frag
	//
	//		// Use shader model 3.0 target, to get nicer looking lighting
	////		#pragma target 3.0
	////		#include "UnityCG.cginc"
	//
	//		sampler2D _MainTex;
	//		sampler2D _Detail;
	//
	//		struct Input {
	//            float4 pos : POSITION;
	//            float3 Z : TEXCOORD0;
	//			float2 uv_MainTex;
	//			float4 screenPos;
	//		};
	//
	////        struct v2f {
	////            float4 pos : SV_POSITION;
	////            float3 color : COLOR0;
	////        };
	////
	////        struct V_IN
	////        {
	////            float4 vertex : POSITION;
	////            float3 normal : NORMAL;
	////            float4 texcoord : TEXCOORD0;
	////        };
	//		fixed4 _Color;
	//
	//		void surf (Input IN, inout SurfaceOutputStandard o) {
	//			// Albedo comes from a texture tinted by color
	//			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
	//
	////			pixelWidth = IN.screenPos.w/180;
	////			pixelHeight = IN.screenPos.h/360;
	//
	//			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;//int2(pixelWidth,pixelHeight);
	//			float widthD = 1 / 90;
	//			float heightD = 1 / 160;
	////			screenUV *= float2(90,160);
	//			screenUV *= float2(9,16);
	//
	//			o.Albedo = c.rgb;
	//			int xP = int(screenUV.x)%2;
	//			int yP = int(screenUV.y)%2;
	//			int value = xP*yP + (xP - 1)*(yP - 1);
	//			//if (screenUV.y < 10)
	//			o.Albedo *= float3(value*6,value*6,value*6);
	//			//if (c.r*c.g*c.b < 1) {
	//			//	o.Albedo *= value;//float3(0, 0, 0);
	//			//} //else {
	//				//o.Albedo = float3(6, 6, 6);
	//			//}
	////			o.Albedo *= tex2D(_Detail,screenUV).rgb * 2;
	////			o.Alpha = c.a;
	//		}
	//
	////        v2f vert (V_IN v) {
	////            v2f o;
	////            float4 oPos = mul(UNITY_MATRIX_MVP, v.vertex);
	////            o.pos = oPos;
	//////            o.Z = oPos.zzz;
	////            return o;
	////        }
	////        half4 frag( v2f i ) : COLOR {
	////            return half4 (i.color, 1);
	////        }
	//		ENDCG
	////		}
	//	}
	//	FallBack "Diffuse"
	//}
	Properties{
		_MainTex("Texture", 2D) = "black" {}
		_NetPattern("Net pattern", 2D) = "black" {}
		_CellSize("Cell Size", Vector) = (0.01, 0.01, 0, 0)
		_Background("Background", 2D) = "green" {}
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200
		
		//GrabPass{ "_PixelationGrabTexture" }

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			v2f vert(appdata_base v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = ComputeGrabScreenPos(o.pos);
				return o;
			}

			sampler2D _MainTex;

			float4 frag(v2f IN) : COLOR{
				float2 steppedUV = IN.uv.xy / IN.uv.w;
				float4 color = tex2D(_MainTex, steppedUV);
				return color;
			}
			ENDCG
		}

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float4 grabUV : TEXCOORD0;
			};

			float4 _CellSize;
			sampler2D _NetPattern;
			sampler2D _Background;

			v2f vert(appdata_base v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.grabUV = ComputeGrabScreenPos(o.pos);
				return o;
			}

			//sampler2D _PixelationGrabTexture;
			sampler2D _MainTex;

			float4 frag(v2f IN) : COLOR{
				float2 steppedUV = IN.grabUV.xy / IN.grabUV.w;
				float4 netColor = tex2D(_NetPattern, steppedUV);
				float4 bgColor = tex2D(_Background, steppedUV);
				steppedUV /= _CellSize.xy;
				steppedUV = round(steppedUV);
				steppedUV *= _CellSize.xy;
				float4 color = tex2D(_MainTex, steppedUV); // float4(0,0,0,0);//
				float has = color.r*color.g*color.b;
				has = round(has);
				color = float4 (has, has, has, 1);
				float netHas = netColor.r*netColor.g*netColor.b;
				if (has == 0)
					if (netHas != 0)
						//color = float4(1,1,1,1);
						color = netColor;
				//color += netColor;
				color *= bgColor;
				return color;
			}
			ENDCG

			//#include "AutoLight.cginc"

			// in v2f struct;
			//LIGHTING_COORDS(0, 1) // replace 0 and 1 with the next available TEXCOORDs in your shader, don't put a semicolon at the end of this line.

			//					  // in vert shader;
			//TRANSFER_VERTEX_TO_FRAGMENT(o); // Calculates shadow and light attenuation and passes it to the frag shader.

			//								//in frag shader;
			//float atten = LIGHT_ATTENUATION(i); // This is a float for your shadow/attenuation value, multiply your lighting value by this to get shadows. Replace i with whatever you've defined your input struct to be called (e.g. frag(v2f [b]i[/b]) : COLOR { ... ).

		}
	}
}
